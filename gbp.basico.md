# Usando GBP

## Instalando gbp
```
apt install gbp-buildpackage
```

## Vendo subcomandos

gbp --list-cmds

man gbp
man gbp.conf

## Alterando conf
/etc/git-buildpackage/gbp.conf

debian-branch = debian/master
pristine-tar = True

## Documentação
https://dep-team.pages.debian.net/deps/dep14/

## Criando repositório git
```
gbp import-dsc arquivo.dsc

git init
gbp import-dscs --debsnap pacote

## Criando orig
```
gbp export-orig
```

## Tag
```
gbp tag
gbp tag --ignore-new

```
## Removendo tag
```
git tag -d debian/0-1.2 ##exemplo
```

## Gerando o changelog

```
gbp dch -i
```

## Gerando orig

gbp export-orig

## Teste CI

```
mkdir debian/tests
vi debian/tests/control
```

Test-Command:
```
autopkgtest . -- null
```

Tag:

```
gbp tag
```

git tag -d debian/1.1-2

## Atualizando versão upstream

```
gbp import-orig ../nova-versão.tar.gz
#gbp import-orig ../6.2.4.tar.gz --no-sign-tags
gbp import-orig --uscan

gbp dch

```

- Subir para o repositório a primeira vez.

```
git remote add origin git@salsa.debian.org:kretcheu/ddrutility.git
git push --all
git push --tags

```
## pacote a partir do tarball upstream

### Pacote
```
wget https://github.com/dbeaver/dbeaver/archive/refs/tags/21.3.2.tar.gz
mkdir dbeaver
cd dbeaver
git init
gbp import-orig ../21.3.2.tar.gz
```

Debianizar
```
debmake -p pacote -u upstrem-version
```
