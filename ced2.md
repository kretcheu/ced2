# Curso Empacotamento Debian (24h) Módulo 2

## Objetivos

Ser capaz…
- de empacotar usando controle de versão git
- de manter pacotes usando git e gpb

Dinâmica:
- conceitos
- laboratórios
- atividades individuais
- oficinas
- material de apoio

## Conteúdo

- git essencial
- gbp
- Incluindo pacote no git
- Atualizando pacote com git
- Clonando pacote já no git
- git branch
- git rebase
- gbp export-orig
- gbp import-orig
- CI/CD

## Atividades

- Serão 4 aulas de 1h30 aproximadamente.
- Participação no grupo do Telegram para tirar dúvidas.

## Material

- Serão disponibilizados os vídeos para participantes.
- Material de apoio composto de slides, guias e links.

## Custo e condições

- R$ 200,00 via pix ou parcelado no cartão via pagseguro.

Pix:
kretcheu@gmail.com

PagSeguro
https://pag.ae/7Y3bdCojs

## Contato

- e-mail: kretcheu@gmail.com
- Telegram: @kretcheu

